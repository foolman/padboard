//
//  UFAppDelegate.m
//  PadBoard
//
//  Created by Németh Gábor on 7/6/12.
//  Copyright (c) 2012 United Force. All rights reserved.
//

#import "UFAppDelegate.h"
#import "JoystickReader.h"
#import "UFPadBoardTableDataSource.h"
#import "UFKeyboardWriter.h"

@implementation UFAppDelegate {
@private
  NSWindow *_padSheet;
}


JoystickReader *reader;

@synthesize window = _window;
@synthesize table = _table;
@synthesize keySheet = _keySheet;
@synthesize padSheet = _padSheet;
@synthesize stateToggle = _stateToggle;
@synthesize eventDictionary = _eventDictionary;

enum {
  IDLE,
  KEY,
  PAD,
  RUN
} mode;
int lastKey;
NSString *lastEvent;
NSURL *lastFile;

UFPadBoardTableDataSource *eventMap;

- (void)dealloc {
  [reader release];
  [eventMap release];
  [_eventDictionary release];
  [lastFile release];
  [super dealloc];
}


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {

  reader = [[[JoystickReader alloc] init] retain];

  if ([reader initializeJoystick]) {
    [reader setDelegate:self];
  };
  eventMap = [[[UFPadBoardTableDataSource alloc] init] retain];

  [_table setDataSource:eventMap];
  mode = IDLE;
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender {
  return YES;
}

- (void)keyDown:(NSEvent *)theEvent {
  NSLog(@"Key pressed, code: %i", [theEvent keyCode]);
  if (mode == KEY) {
    lastKey = [theEvent keyCode];
    mode = PAD;
    [NSApp endSheet:_keySheet];
    [NSApp beginSheet:_padSheet
       modalForWindow:_window
        modalDelegate:self
       didEndSelector:@selector(didEndSheet:returnCode:contextInfo:)
          contextInfo:_window];

  }
}

- (void)joystickEvent:(NSString *)eventSource isDown:(BOOL)down {
  if (down && mode == PAD) {
    lastEvent = eventSource;
    [NSApp endSheet:_padSheet];
    mode = IDLE;
    [eventMap addKey:lastKey forEvent:lastEvent];
    [_table reloadData];
  } else if (mode == RUN) {
    NSNumber *code = [_eventDictionary objectForKey:eventSource];
    if (Nil != code) {
      int iCode = [code intValue];
      if (down) [UFKeyboardWriter postKeyDown:iCode];
      else [UFKeyboardWriter postKeyUp:iCode];
    }
  }
}

- (BOOL)acceptsFirstResponder {
  return YES;
}

- (BOOL)becomeFirstResponder {
  return YES;
}

- (IBAction)addEvent:(NSButton *)sender {
  [_window makeFirstResponder:self];
  mode = KEY;
  [NSApp beginSheet:_keySheet
     modalForWindow:_window
      modalDelegate:self
     didEndSelector:@selector(didEndSheet:returnCode:contextInfo:)
        contextInfo:_window];
}

- (IBAction)removeEvent:(NSButton *)sender {
  NSLog(@"Remove received");
  [eventMap removeRow:[_table selectedRow]];
  [_table reloadData];
}

- (IBAction)cancelProgramming:(NSButton *)sender {
  if (mode == KEY) [NSApp endSheet:_keySheet];
  if (mode == PAD) [NSApp endSheet:_padSheet];
  mode = IDLE;
}


- (void)didEndSheet:(NSWindow *)sheet
         returnCode:(NSInteger)returnCode
        contextInfo:(void *)contextInfo {
  [sheet orderOut:self];
}

- (IBAction)toggleState:(id)sender {
  if (IDLE == mode) {
    mode = RUN;
    [_eventDictionary release];
    _eventDictionary = [[eventMap asDictionary] retain];
    [_stateToggle setTitle:@"Stop"];
  } else if (RUN == mode) {
    mode = IDLE;
    [_stateToggle setTitle:@"Start"];
  }
}


- (BOOL)loadFromURL:(NSURL *)file {
  NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfURL:file];
  if (nil != dictionary) {
    if ([eventMap fromDictionary:dictionary]) {
      if (RUN == mode) {
        [self toggleState:nil];
      }
      [_table reloadData];
      [file retain];
      [lastFile release];
      lastFile = file;
      return YES;
    }
  }
  return NO;
}

- (Boolean)readFile:(NSURL *)file {
  const BOOL loadSucceded = [self loadFromURL:file];
  if (loadSucceded)
    [[NSDocumentController sharedDocumentController] noteNewRecentDocumentURL:file];
  return loadSucceded;
}

- (void)openDocument:(id)sender {
  NSOpenPanel *panel = [NSOpenPanel openPanel];

  [panel setAllowedFileTypes:[NSArray arrayWithObject:fileType]];
  [panel setAllowsOtherFileTypes:YES];
  int result = [panel runModal];
  if (NSFileHandlingPanelOKButton == result) {
    [self readFile:[panel URL]];
  }
}

- (void)saveToURL:(NSURL *)file {
  if (![[file pathExtension] isEqualToString:fileType]) {
    file = [file URLByAppendingPathExtension:fileType];
  }
  NSLog(@"Writing file %@", file);
  if (![[eventMap asDictionary] writeToURL:file atomically:YES]) {
    NSLog(@"Error writing file %@", file);
  };
  [[NSDocumentController sharedDocumentController] noteNewRecentDocumentURL:file];
  [file retain];
  [lastFile release];
  lastFile = file;
}

- (void)saveDocument:(id)sender {
  if (nil != lastFile) {
    [self saveToURL:lastFile];
    return;
  }
  NSSavePanel *panel = [NSSavePanel savePanel];
  int result = [panel runModal];
  if (NSFileHandlingPanelOKButton != result) {
    NSURL *file = [panel URL];
    [self saveToURL:file];
  }

}

- (BOOL)application:(NSApplication *)sender openFile:(NSString *)filename {
  if ([filename rangeOfString:@"://"].location == NSNotFound)
    filename = [@"file://" stringByAppendingString:filename];
  NSLog(@"Opening file %@", filename);
  NSURL *url = [NSURL URLWithString:filename];

  if (nil == url) return NO;
  if ([[url pathExtension] caseInsensitiveCompare:fileType] != NSOrderedSame) {
    NSLog(@"Invalid url");
    return NO;
  }
  return [self readFile:url];
}

@end
