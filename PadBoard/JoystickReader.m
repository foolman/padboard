//
// Created by foolman on 7/10/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <DDHidLib/DDHidLib.h>
#import "JoystickReader.h"

/*
the plan:
0. create store for axes.
1. convert analog changes to discrete events

 */

@implementation JoystickReader
@synthesize delegate;


DDHidJoystick *aJoy;

typedef struct {
  int xValue;
  int yValue;
  int *otherValues;
} AxisValues;

AxisValues **stickValues;

- (id)init {
  self = [super init];
  if (self) {
  }

  return self;
}

- (bool)initializeJoystick {
  NSArray *joysticks = [DDHidJoystick allJoysticks];

  if (0 == [joysticks count]) {
    NSLog(@"No joysticks found");
    return NO;
  }
  aJoy = [joysticks objectAtIndex:0];
  [aJoy retain];
  NSLog(@"Joystick found %@ %@", [aJoy manufacturer], [aJoy productName]);
  int sticks = [aJoy countOfSticks];
  stickValues = (AxisValues **) malloc(sizeof(AxisValues *) * sticks);

  for (unsigned int x = 0; x < sticks; x++) {
    DDHidJoystickStick *stick = [aJoy objectInSticksAtIndex:x];
    int axes = [stick countOfStickElements];
    AxisValues *tmp = (AxisValues *) malloc(sizeof(AxisValues));

    tmp->otherValues = malloc(sizeof(int) * axes);
    for (int y = 0; y < axes; y++) {
      tmp->otherValues[y] = 0;
    }
    tmp->xValue = 0;
    tmp->yValue = 0;
    stickValues[x] = tmp;
  }

  [aJoy setDelegate:self];
  [aJoy startListening];
  return YES;
}

bool isUp(int value) {
  return value > DDHID_JOYSTICK_VALUE_MAX * 0.5;
}

BOOL isItAnEvent(int oldVal, int value, BOOL *down) {
  if (isUp(oldVal) && !isUp(value)) {
    *down = NO;
    return YES;
  }
  if (!isUp(oldVal) && isUp(value)) {
    *down = YES;
    return YES;
  }
  return NO;
}


- (void)ddhidJoystick:(DDHidJoystick *)joystick stick:(unsigned int)stick
             xChanged:(int)value {
  BOOL down;
  NSString *direction = value < 0 ? @"left" : @"right";

  value = abs(value);
  if (isItAnEvent(stickValues[stick]->xValue, value, &down)) {
    NSString *event =
        [NSString stringWithFormat:@"stick%i%@", stick, direction];

    if ([delegate respondsToSelector:@selector(joystickEvent:isDown:)])
      [delegate joystickEvent:event isDown:down];

  }
  stickValues[stick]->xValue = value;
}


- (void)ddhidJoystick:(DDHidJoystick *)joystick stick:(unsigned int)stick
             yChanged:(int)value {
  BOOL down;
  NSString *direction = value < 0 ? @"up" : @"down";
  value = abs(value);

  if (isItAnEvent(stickValues[stick]->yValue, value, &down)) {
    NSString *event =
        [NSString stringWithFormat:@"stick%i%@", stick, direction];

    if ([delegate respondsToSelector:@selector(joystickEvent:isDown:)])
      [delegate joystickEvent:event isDown:down];
  }
  stickValues[stick]->yValue = value;
}

- (void)ddhidJoystick:(DDHidJoystick *)joystick
                stick:(unsigned int)stick
            otherAxis:(unsigned int)otherAxis
         valueChanged:(int)value {
  DDHidElement *emt =
      [[joystick objectInSticksAtIndex:stick]
          objectInStickElementsAtIndex:otherAxis];

  NSString *direction = value < 0 ? @"neg" : @"pos";
  if ([emt minValue] >= 0) {
    value = (value - DDHID_JOYSTICK_VALUE_MIN) >> 1;
    direction = @"abs";
  } else {
    value = abs(value);
  }

  BOOL down;

  if (isItAnEvent(stickValues[stick]->otherValues[otherAxis], value, &down)) {
    NSString *event =
        [NSString stringWithFormat:@"stick%iaxis%i%@",
                                   stick, otherAxis, direction];
    if ([delegate respondsToSelector:@selector(joystickEvent:isDown:)])
      [delegate joystickEvent:event isDown:down];
  }
  stickValues[stick]->otherValues[otherAxis] = value;

}

- (void)ddhidJoystick:(DDHidJoystick *)joystick
                stick:(unsigned int)stick
            povNumber:(unsigned int)povNumber
         valueChanged:(int)value {
  NSLog(@"stick %i pov %i moved to %i", stick, povNumber, value);
}

- (void)ddhidJoystick:(DDHidJoystick *)joystick
           buttonDown:(unsigned int)buttonNumber {
  NSString *event = [NSString stringWithFormat:@"button%i", buttonNumber];
  if ([delegate respondsToSelector:@selector(joystickEvent:isDown:)])
    [delegate joystickEvent:event isDown:YES];

}

- (void)ddhidJoystick:(DDHidJoystick *)joystick
             buttonUp:(unsigned int)buttonNumber {
  NSString *event = [NSString stringWithFormat:@"button%i", buttonNumber];
  if ([delegate respondsToSelector:@selector(joystickEvent:isDown:)])
    [delegate joystickEvent:event isDown:NO];

}

- (void)dealloc {
  [delegate release];
  for (int x = 0; x < [aJoy countOfSticks]; x++) free(stickValues[x]->otherValues);
  free(stickValues);
  [aJoy release];
  [super dealloc];
}


@end