//
// Created by foolman on 7/10/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

@interface NSObject (JoystickReaderDelegate)
- (void)joystickEvent:(NSString *)eventSource isDown:(BOOL)down;
@end


@interface JoystickReader : NSObject {
  id delegate;
}
@property(nonatomic, retain) id delegate;

- (bool)initializeJoystick;
@end


