//
//  main.m
//  PadBoard
//
//  Created by Németh Gábor on 7/6/12.
//  Copyright (c) 2012 United Force. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
