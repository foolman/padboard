//
//  UFPadBoardTableDataSource.h
//  PadBoard
//
//  Created by Németh Gábor on 7/17/12.
//  Copyright (c) 2012 United Force. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UFPadBoardTableDataSource : NSObject <NSTableViewDataSource>
@property(retain) NSMutableArray *eventMap;

- (void)addKey:(int)keycode forEvent:(NSString *)event;

- (void)removeRow:(NSInteger)row;

- (NSDictionary *)asDictionary;

- (BOOL)fromDictionary:(NSDictionary *)dictionary;
@end
