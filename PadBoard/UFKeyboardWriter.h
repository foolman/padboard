//
//  UFKeyboardWriter.h
//  PadBoard
//
//  Created by Németh Gábor on 7/6/12.
//  Copyright (c) 2012 United Force. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UFKeyboardWriter : NSObject
+ (void)postKeyDown:(int)keyCode;

+ (void)postKeyUp:(int)keyCode;
@end
