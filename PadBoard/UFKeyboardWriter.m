//
//  UFKeyboardWriter.m
//  PadBoard
//
//  Created by Németh Gábor on 7/6/12.
//  Copyright (c) 2012 United Force. All rights reserved.
//

#import "UFKeyboardWriter.h"

@implementation UFKeyboardWriter
+ (void)postKeyDown:(int)keyCode {

  AXUIElementRef axSystemWideElement = AXUIElementCreateSystemWide();
  AXError err =
      AXUIElementPostKeyboardEvent(axSystemWideElement, 0, keyCode, true);
  if (err != kAXErrorSuccess) NSLog(@"Code not sent, error %i", err);
}

+ (void)postKeyUp:(int)keyCode {
  AXUIElementRef axSystemWideElement = AXUIElementCreateSystemWide();
  AXError err =
      AXUIElementPostKeyboardEvent(axSystemWideElement, 0, keyCode, false);
  if (err != kAXErrorSuccess) NSLog(@"Code not sent, error %i", err);
}

@end
