//
//  UFAppDelegate.h
//  PadBoard
//
//  Created by Németh Gábor on 7/6/12.
//  Copyright (c) 2012 United Force. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <DDHidLib/DDHidJoystick.h>

static NSString *const fileType = @"padboard";

@interface UFAppDelegate : NSResponder <NSApplicationDelegate>

@property(assign) IBOutlet NSWindow *window;
@property(assign) IBOutlet NSTableView *table;
@property(assign) IBOutlet NSWindow *keySheet;
@property(assign) IBOutlet NSWindow *padSheet;
@property(assign) IBOutlet NSButton *stateToggle;
@property(retain) NSDictionary *eventDictionary;

- (IBAction)addEvent:(NSButton *)sender;

- (IBAction)removeEvent:(NSButton *)sender;

- (IBAction)cancelProgramming:(NSButton *)sender;

- (IBAction)toggleState:(id)sender;

@end
