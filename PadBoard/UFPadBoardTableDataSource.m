//
//  UFPadBoardTableDataSource.m
//  PadBoard
//
//  Created by Németh Gábor on 7/17/12.
//  Copyright (c) 2012 United Force. All rights reserved.
//

#import "UFPadBoardTableDataSource.h"


@implementation UFPadBoardTableDataSource {
@private
  NSMutableArray *_eventMap;
}

@synthesize eventMap = _eventMap;

- (id)init {
  self = [super init];
  if (self) {
    _eventMap = [[NSMutableArray array] retain];
  }

  return self;
}


- (int)numberOfRowsInTableView:(NSTableView *)tableView {
  return [_eventMap count];
}

- (id)          tableView:(NSTableView *)tableView
objectValueForTableColumn:(NSTableColumn *)tableColumn
                      row:(NSInteger)row {
  NSDictionary *record = [_eventMap objectAtIndex:row];
  return [record objectForKey:[tableColumn identifier]];
}

- (void)dealloc {
  [_eventMap release];
  [super dealloc];
}

- (void)addKey:(int)keyCode forEvent:(NSString *)event {
  NSMutableDictionary *k =
      [[NSMutableDictionary dictionaryWithCapacity:2] autorelease];
  [k setObject:event forKey:@"PadEvent"];
  [k setObject:[NSNumber numberWithInt:keyCode] forKey:@"KeyCode"];
  [_eventMap addObject:[NSDictionary dictionaryWithDictionary:k]];
}

- (void)removeRow:(NSInteger)row {
  if (-1 == row) [_eventMap removeLastObject];
  else [_eventMap removeObjectAtIndex:row];
}

- (NSDictionary *)asDictionary {
  NSUInteger count = [_eventMap count];
  NSMutableDictionary *tempDict =
      [NSMutableDictionary dictionaryWithCapacity:count];
  for (NSDictionary *row in _eventMap) {
    [tempDict setObject:[row objectForKey:@"KeyCode"]
        forKey:[row objectForKey:@"PadEvent"]];
  }
  return [NSDictionary dictionaryWithDictionary:tempDict];
}

- (BOOL)fromDictionary:(NSDictionary *)dictionary {
  NSMutableArray *tmpArray =
      [NSMutableArray arrayWithCapacity:[dictionary count]];
  NSArray *keyArray = [NSArray arrayWithObjects:@"PadEvent", @"KeyCode", nil];

  for (NSString *key in dictionary) {
    NSArray *valueArray =
        [NSArray arrayWithObjects:key, [dictionary objectForKey:key], nil];
    NSDictionary *tmpDict =
        [NSDictionary dictionaryWithObjects:valueArray forKeys:keyArray];
    [tmpArray addObject:[NSDictionary dictionaryWithDictionary:tmpDict]];
  }
  [_eventMap release];
  _eventMap = [tmpArray retain];
  return YES;
}
@end
